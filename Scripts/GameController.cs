﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private BallController currentBall;
    static private int score1 = 0;
    static private int score2 = 0;
    private bool gameOver = false;

    public GameObject ballPrefab;
    public Text score1Text;
    public Text score2Text;
    public Text winnerText;
    public Text restartText;
    public float scoreCoordinates = 9.0f;
    public int winCondition = 3;

    // Start is called before the first frame update
    void Start()
    {
        InitGame();
        // Debug.Log("Game started.");
        SpawnBall();
    }

    void SpawnBall()
    // Used to spawn a new ball into play
    {
        // Remove the current ball in play
        if (currentBall != null)
        {
            Destroy(currentBall.gameObject);
        }

        // Update the dislayed score
        UpdateScore();

        if (!gameOver)
        {
            // Creates the new game ball
            GameObject ballInstance = Instantiate(ballPrefab, transform);
            currentBall = ballInstance.GetComponent<BallController>();
            currentBall.transform.position = Vector3.zero;

            // Check that new ball spawned
            if (currentBall != null)
            {
                // Debug.Log("New ball spawned.");
            }
            else
            {
                // Debug.Log("Failed to spawn new ball.");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (currentBall != null)
        {
            if (currentBall.transform.position.x > scoreCoordinates)
            {
                // player 1 scored
                // Debug.Log("Player 1 Scored");
                score1++;
                SpawnBall();
            }

            if (currentBall.transform.position.x < -scoreCoordinates)
            {
                // player 2 scored
                // Debug.Log("Player 2 Scored");
                score2++;
                SpawnBall();
            }
        }

        if (gameOver)
        {
            // If game over, restart the game when Space is pressed
            if (Input.GetKeyDown("space"))
            {
                SceneManager.LoadScene("Game");
            }
        }
    }

    void UpdateScore()
        // Updates displayed score text object
    {
        score1Text.text = score1.ToString();
        score2Text.text = score2.ToString();
       // Debug.Log("Score updated");

        CheckWinCondition();
    }

    void CheckWinCondition()
        // Check to see if the games win condition has been met
    {
        if (score1 == winCondition)
        {
            // Player 1 wins
            // Debug.Log("Player 1 Wins");
            SetWinnerText("Player 1");
            gameOver = true;
        } else if (score2 == winCondition)
        {
            // Player 2 wins
            // Debug.Log("Player 2 Wins");
            SetWinnerText("Player 2");
            gameOver = true;
        }
    }

    void SetWinnerText(string player)
        // Set winner Text fields
    {
        string winner = player;
        winnerText.text = winner + " Wins!";
        restartText.text = "Press [Space] to Start a New Game";
    }

    void InitGame()
        // Reset settings when a new game starts
    {
        gameOver = false;
        score1 = 0;
        score2 = 0;
        winnerText.text = string.Empty;
        restartText.text = string.Empty;
    }
}


