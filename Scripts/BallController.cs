﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private Rigidbody2D ballRigidBody;

    public float minXSpeed = 1f;
    public float maxXSpeed = 2f;
    public float minYSpeed = 1f;
    public float maxYSpeed = 2f;
    public float difficultyMultiplier = 1.2f;

    // Start is called before the first frame update
    void Start()
    {
        ballRigidBody = GetComponent<Rigidbody2D>();
        ballRigidBody.velocity = new Vector2(
            Random.Range(minXSpeed, maxXSpeed) * (Random.value > 0.5f ? -1 : 1), 
            Random.Range(minYSpeed, maxYSpeed) * (Random.value > 0.5f ? -1 : 1) // 50:50 to start moving up or down
            );
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Limit")
        // hit boundry
        {
            GetComponent<AudioSource>().Play();
            if (collision.transform.position.y > transform.position.y && ballRigidBody.velocity.y > 0)
            // bounce off top boundry
            // ball moving up
            {
                ballRigidBody.velocity = new Vector2(ballRigidBody.velocity.x, -ballRigidBody.velocity.y);
            } else if (collision.transform.position.y < transform.position.y && ballRigidBody.velocity.y < 0)
            // bounce off bottom boundry
            // ball moving down
            {
                ballRigidBody.velocity = new Vector2(ballRigidBody.velocity.x, -ballRigidBody.velocity.y);
            }
        }

        if(collision.tag == "Paddle")
        {
            GetComponent<AudioSource>().Play();
            if (collision.transform.position.x < transform.position.x && ballRigidBody.velocity.x < 0)
            // ball moving left
            {
                ballRigidBody.velocity = new Vector2(-ballRigidBody.velocity.x * difficultyMultiplier, ballRigidBody.velocity.y * difficultyMultiplier);
            } else if (collision.transform.position.x > transform.position.x && ballRigidBody.velocity.x > 0)
            // ball moving right
            {
                ballRigidBody.velocity = new Vector2(-ballRigidBody.velocity.x * difficultyMultiplier, ballRigidBody.velocity.y * difficultyMultiplier);
            }
        }
    }
}
